// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: BSD-2-Clause

fn main() {
    cxx_build::bridge("src/lib.rs").compile("keepass")
}
