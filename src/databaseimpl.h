// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>

#include "lib.rs.h"
#include <optional>

class DatabaseImpl : public QObject
{
    Q_OBJECT
public:
    DatabaseImpl();

    void open(const QUrl &path, const QString &password);

    QString title(int index) const;
    QString username(int index) const;
    QString password(int index) const;

    int size() const;

Q_SIGNALS:
    void finished();

private:
    std::optional<RustDB> m_database = std::nullopt;

};
