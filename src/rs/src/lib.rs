// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

use keepass::Database;
use std::fs::File;
use std::ops::Deref;
use ffi::RustDB;

pub struct DB {
    database: Option<Database>,
}

impl Deref for DB {
    type Target = Option<Database>;
    fn deref(&self) -> &Self::Target {
        &self.database
    }
}

fn open_database(path: &str, password: &str) -> RustDB {
    if let Ok(mut file) = File::open(path) {
        if let Ok(db) = Database::open(&mut file, Some(password), None) {
            RustDB { database: Box::new( DB { database: Some(db) } ), error: ffi::Error::Success }
        } else {
            RustDB { database: Box::new(DB { database: None}), error: ffi::Error::Decryption }
        }
    } else {
        RustDB { database: Box::new(DB { database: None }), error: ffi::Error::File}
    }
}

fn title_for_index<'a>(database: &'a RustDB, index: usize) -> &'a str {
    if let Some(db) = &**database.database {
        let (_, value) = db.root.entries.iter().nth(index).unwrap();
        value.get_title().unwrap()
    } else {
        ""
    }
}

fn username_for_index<'a>(database: &'a RustDB, index: usize) -> &'a str {
    if let Some(db) = &**database.database {
        let (_, value) = db.root.entries.iter().nth(index).unwrap();
        value.get_username().unwrap()
    } else {
        ""
    }
}

fn password_for_index<'a>(database: &'a RustDB, index: usize) -> &'a str {
    if let Some(db) = &**database.database {
        let (_, value) = db.root.entries.iter().nth(index).unwrap();
        value.get_password().unwrap()
    } else {
        ""
    }
}

fn database_size(database: &RustDB) -> usize {
    if let Some(db) = &**database.database {
        db.root.entries.len()
    } else {
        0
    }
}

#[cxx::bridge]
mod ffi {
    pub struct RustDB {
        database: Box<DB>,
        error: Error,
    }
    enum Error {
        Success,
        File,
        Decryption,
    }
    extern "Rust" {
        type DB;

        fn open_database(path: &str, password: &str) -> RustDB;
        unsafe fn title_for_index<'a>(database: &'a RustDB, index: usize) -> &'a str;
        unsafe fn username_for_index<'a>(database: &'a RustDB, index: usize) -> &'a str;
        unsafe fn password_for_index<'a>(database: &'a RustDB, index: usize) -> &'a str;
        fn database_size(database: &RustDB) -> usize;
    }
}
