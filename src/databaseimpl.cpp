// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "databaseimpl.h"

#include <QUrl>
#include <QTimer>
#include <QThread>
#include <QDebug>

DatabaseImpl::DatabaseImpl()
{
}

void DatabaseImpl::open(const QUrl &path, const QString &password)
{
    auto thread = new QThread();
    moveToThread(thread);
    QMetaObject::invokeMethod(this, [=](){
        m_database = open_database(path.toLocalFile().toStdString(), password.toStdString());
        Q_EMIT finished();
    }, Qt::QueuedConnection);
    thread->start();
}

QString DatabaseImpl::password(int index) const
{
    return QString::fromStdString(std::string(password_for_index(*m_database, index)));
}

QString DatabaseImpl::username(int index) const
{
    return QString::fromStdString(std::string(username_for_index(*m_database, index)));
}

QString DatabaseImpl::title(int index) const
{
    return QString::fromStdString(std::string(title_for_index(*m_database, index)));
}

int DatabaseImpl::size() const
{
    if(m_database) {
        return database_size(*m_database);
    } else {
        return 0;
    }
}
