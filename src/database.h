// SPDX-FileCopyrightText: 2021 Tobias Fella <fella@posteo.de>
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once

#include <QObject>
#include <QAbstractListModel>
#include <QUrl>

#include <KAboutData>

#include "databaseimpl.h"

class Database : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(KAboutData aboutData READ aboutData CONSTANT)
    Q_PROPERTY(bool decrypting READ decrypting NOTIFY decryptingChanged)
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)

public:
    enum RoleNames {
        Title = Qt::UserRole + 1,
        Username,
        Password,
    };
    Database(QObject *parent = nullptr);

    Q_INVOKABLE void open(const QString &password);
    Q_INVOKABLE void copyPassword(const QString &password);

    int rowCount(const QModelIndex &index) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

    KAboutData aboutData() const;
    bool decrypting() const;
    void setDecrypting(bool decrypting);

    QString path() const;
    void setPath(const QString &path);

Q_SIGNALS:
    void decryptingChanged();
    void pathChanged();

private:
    DatabaseImpl *m_impl;
    bool m_decrypting;
    QString m_path;
};
